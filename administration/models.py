from django.db import models
from django.utils.translation import ugettext as _
from torres3000.utils import PhoneNumberRegexValidator


class Apartment(models.Model):
    name = models.CharField(max_length=100, verbose_name='Departamento')
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def delete(self):
        self.deleted = True
        self.save()

    def hard_delete(self):
        super(Apartment, self).delete()

class Infotype(models.Model):
    code = models.CharField(max_length=10, verbose_name='Código')
    name = models.CharField(max_length=100, verbose_name='Conociste')
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def delete(self):
        self.deleted = True
        self.save()

    def hard_delete(self):
        super(Infotype, self).delete()

class Cotizacion(models.Model):
    name = models.CharField(max_length=100, verbose_name='NOMBRE')
    last_name = models.CharField(max_length=100, verbose_name='APELLIDO')
    email = models.CharField(max_length=100, verbose_name='CORREO ELECTRÓNICO')
    number = models.CharField( max_length=16, blank=True, null=True, validators=[PhoneNumberRegexValidator], verbose_name='CELULAR')
    description = models.TextField(blank=True, null=True, verbose_name=u'MENSAJE')
    apartment = models.ForeignKey(Apartment, null=True,  on_delete=models.DO_NOTHING, verbose_name='ESTOY INTERESADO EN')
    info_type = models.ForeignKey(Infotype, null=True, on_delete=models.DO_NOTHING, verbose_name=u'¿CÓMO NOS CONOCISTE?')

    date_creation = models.DateTimeField(auto_now_add=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def delete(self):
        self.deleted = True
        self.save()

    def hard_delete(self):
        super(Cotizacion, self).delete()


    #def send_mail(self):
        #email_template_name = 'email_account_new.html'
#
        #context = {
            #'domain': "%s%s" % (EMAIL_PREFIX, EMAIL_DOMAIN),  # or your domain
            #'site_name': 'Torres3000',
            #'protocol': 'http',
            #'email':self.email,
            #'first_name':self.name,
            #'last_name':self.last_name,
        #}
#
        #html_content = loader.render_to_string(email_template_name, context)
        #subject='Mi doctor 24/7 - Cuenta'
#
        #email_message = EmailMultiAlternatives(subject, html_content, to=[self.email], from_email=DEFAULT_FROM_EMAIL)
#
        #try:
            #email_message.send(fail_silently=False)
            #return True
        #except Exception as e:
            #import traceback
            #traceback.print_exc()
            #print(e)
            #raise Http404
            #return False

    
