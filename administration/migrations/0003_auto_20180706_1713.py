# Generated by Django 2.0.7 on 2018-07-06 22:13

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0002_auto_20180705_1118'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cotizacion',
            name='description',
            field=models.TextField(blank=True, null=True, verbose_name='MENSAJE'),
        ),
        migrations.AlterField(
            model_name='cotizacion',
            name='last_name',
            field=models.CharField(max_length=100, verbose_name='APELLIDO'),
        ),
        migrations.AlterField(
            model_name='cotizacion',
            name='name',
            field=models.CharField(max_length=100, verbose_name='NOMBRE'),
        ),
        migrations.AlterField(
            model_name='cotizacion',
            name='number',
            field=models.CharField(max_length=16, null=True, validators=[django.core.validators.RegexValidator(message='Número telefónico inválido.', regex='^[\\+]?[(]?[0-9]{3}[)]?[-\\s\\.]?[0-9]{3}[-\\s\\.]?[0-9]{3,6}$')], verbose_name='CELULAR'),
        ),
    ]
