1.- Instalar Entorno virtual
    brew install pyenv
    brew install pyenv-virtualenv
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
    echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bash_profile
    echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bash_profile
    pyenv install 3.6.4
    pyenv global 3.6.4

pyenv versions

2.- Crear el Entorno virtual
    pyenv virtualenv 3.6.4 venv_torres3000

3.- Activar el entorno virtual
    pyenv activate venv_torres3000

4.- Instalar Requerimentes
    pip install -r requirements.txt

5.- Migrar base de datos
    python manage.py migrate

6.- Crear superusuario
    python manage.py createsuperuser --email torres3000@torres30002.com.ec --username torre3000_admin
    # 88f878dbbcb1b7c6d8952b3ab41f927aa0902e8e
7.- SSH
    ssh -i "midoctor247.pem" centos@ec2-18-191-25-54.us-east-2.compute.amazonaws.com


