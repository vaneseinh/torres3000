from django.views.generic import View, FormView, TemplateView, CreateView
from django.utils.translation import ugettext as _
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib import messages

from torres3000.settings import EMAIL_PREFIX, EMAIL_DOMAIN, DEFAULT_FROM_EMAIL, EMAIL_HOST_USER
from django.core.mail import EmailMultiAlternatives
from django.template import loader

from torres3000.forms import CotizacionForm
from administration.models import Cotizacion, Apartment, Infotype


def send_mail(request):
    email_template_name = 'email_cotiza.html'
    apartment_name = ""
    info_name = ""

    if request.POST['apartment']:
        apartment = Apartment.objects.filter(id=request.POST['apartment']).values('name').first()
        apartment_name = apartment['name']

    if request.POST['info_type']:
        info = Infotype.objects.filter(id=request.POST['info_type']).values('name').first()
        info_name = info['name']
    
    context = {
        'domain': "%s%s" % (EMAIL_PREFIX, EMAIL_DOMAIN),  # or your domain
        'site_name': 'Torres3000',
        'protocol': 'http',
        'email':request.POST['email'],
        'first_name':request.POST['name'],
        'last_name':request.POST['last_name'],
        'apartment':apartment_name,
        'info_type':info_name,
        'description':request.POST['description'],
        'number':request.POST['number'],
    }

    from_email = request.POST['email']
    to_email = DEFAULT_FROM_EMAIL

    #from_email = EMAIL_HOST_USER
    #to_email = [request.POST['email'] , 'estefania.heredia@cti.espol.edu.ec']

    html_content = loader.render_to_string(email_template_name, context)
    subject='Torres3000 - Cotización'

    try:
        email_message = EmailMultiAlternatives(subject, html_content, to=to_email, from_email=from_email)   
        email_message.attach_alternative(html_content, "text/html") 
        email_message.mixed_subtype = 'related'

        email_message.send(fail_silently=False)

        return True
    except Exception as e:
        import traceback
        traceback.print_exc()
        print(e)
        return False


class HomeView(CreateView):
    """docstring for HomeView"""
    model = Cotizacion
    template_name = "index.html"

    def send_mail(self): send_mail()

    def form_valid(self, form):
        return super(HomeView, self).form_valid(form)

    def get(self, request, *args, **kwargs):
        data = {
            'title': _(u'Puerto Mocolí'),
            'form': CotizacionForm(),
            'is_cotiza_page' : False,
            }
        return render(request, self.template_name, data)

    def post(self, request, *arg, **kwargs):
        form = CotizacionForm(request.POST)
        data = {}
        data = {'title':  _(u'Puerto Mocolí'),
                'form': form,
                'is_cotiza_page' : False, }

        try:
            if form.is_valid():
                form.save()
                if send_mail(request):
                    messages.success(request, _(u'Se ha enviado correctamente su solicitud.'))
                    return redirect("index")
                else:
                    messages.error(request, _(u'Existe un error al enviar su solicitud.'))
                    return render(request, self.template_name, data)
            else:
                messages.error(request, _(u'Existe al guardar su información.'))
                return render(request, self.template_name, data)
        except Exception as e:
            import traceback;
            traceback.print_exc()
            print(e)
        

class ApartmentsView(CreateView):
    """docstring for ApartmentsView"""
    model = Cotizacion
    template_name = "apartments.html"

    def send_mail(self): send_mail()

    def form_valid(self, form):
        return super(ApartmentsView, self).form_valid(form)

    def get(self, request, *args, **kwargs):
        data = {
            'title': _(u'Departamentos'),
            'form': CotizacionForm(),
            'is_cotiza_page' : False,
            }
        return render(request, self.template_name, data)

    def post(self, request, *arg, **kwargs):
        form = CotizacionForm(request.POST)
        data = {}
        data = {'title': _(u'Departamentos'),
                'form': form,
                'is_cotiza_page' : False, }

        try:
            if form.is_valid():
                form.save()
                if send_mail(request):
                    messages.success(request, _(u'Se ha enviado correctamente su solicitud.'))
                    return redirect("index")
                else:
                    messages.error(request, _(u'Existe un error al enviar su solicitud.'))
                    return render(request, self.template_name, data)
            else:
                messages.error(request, _(u'Existe al guardar su información.'))
                return render(request, self.template_name, data)
        except Exception as e:
            import traceback;
            traceback.print_exc()
            print(e)


class ContactView(TemplateView):
    """docstring for ApartmentsView"""
    template_name = "contact.html"

    def get(self, request):
        return super(ContactView, self).get(request)

    def get_context_data(self, **kwargs):
        context = super(ContactView, self).get_context_data(**kwargs)
        context['title'] = _(u'Contacto')
        return context


class CotizaView(CreateView):
    """docstring for ApartmentsView"""
    model = Cotizacion
    template_name = "cotiza.html"

    def send_mail(self): send_mail()

    def form_valid(self, form):
        return super(CotizaView, self).form_valid(form)

    def get(self, request, *args, **kwargs):
        data = {
            'title': _(u'Cotización'),
            'form': CotizacionForm(),
            'is_cotiza_page' : True,
            }
        return render(request, self.template_name, data)

    def post(self, request, *arg, **kwargs):
        form = CotizacionForm(request.POST)
        data = {}
        data = {'title': _(u'Cotización'),
                'form': form,
                'is_cotiza_page' : False, }

        try:
            if form.is_valid():
                form.save()
                if send_mail(request):
                    messages.success(request, _(u'Se ha enviado correctamente su solicitud.'))
                    return redirect("index")
                else:
                    messages.error(request, _(u'Existe un error al enviar su solicitud.'))
                    return render(request, self.template_name, data)
            else:
                messages.error(request, _(u'Existe al guardar su información.'))
                return render(request, self.template_name, data)
        except Exception as e:
            import traceback;
            traceback.print_exc()
            print(e)


class PromotoraView(TemplateView):
    """docstring for PromotoraView"""
    template_name = "promotora.html"

    def get(self, request):
        return super(PromotoraView, self).get(request)

    def get_context_data(self, **kwargs):
        context = super(PromotoraView, self).get_context_data(**kwargs)
        context['title'] = _(u'Promotora')
        return context


class ConstructoraView(TemplateView):
    """docstring for ConstructoraView"""
    template_name = "constructora.html"

    def get(self, request):
        return super(ConstructoraView, self).get(request)

    def get_context_data(self, **kwargs):
        context = super(ConstructoraView, self).get_context_data(**kwargs)
        context['title'] = _(u'Constructora')
        return context