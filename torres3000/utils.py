from django.utils.translation import ugettext as _
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError

error_messages = {
    'required': _(u"El campo es requerido."),
}


PhoneNumberRegexValidator = RegexValidator(
    regex=r'^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,6}$',
    message=_(u"Número telefónico inválido.")
)