# -*- coding: utf-8 -*-
from os.path import basename
from django.utils.translation import ugettext as _
from django import forms
from django.core.exceptions import ValidationError
from torres3000.utils import error_messages

from administration.models import Apartment, Infotype, Cotizacion

class CotizacionForm(forms.ModelForm):
    email = forms.EmailField(required=True, label='CORREO ELECTRÓNICO')
    apartment = forms.ModelChoiceField(queryset=Apartment.objects.filter(deleted=False).all(), label="ESTOY INTERESADO EN", empty_label='ESTOY INTERESADO EN', required=False)
    description = forms.CharField(widget=forms.Textarea, label='MENSAJE')
    info_type = forms.ModelChoiceField(queryset=Infotype.objects.filter(deleted=False).all(),  label="¿CÓMO NOS CONOCISTE?", empty_label='¿CÓMO NOS CONOCISTE?', required=True)

    class Meta:
        model = Cotizacion
        fields = (
            'name',
            'last_name',
            'email',
            'number',
            'description',
            'apartment',
            'info_type',
        )

    def __init__(self, *args, **kwargs):
        super(CotizacionForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].error_messages = error_messages
            self.fields[field].widget.attrs['class'] = ' uk-input'
            self.fields[field].widget.attrs['placeholder'] =  self.fields[field].label

        self.fields['description'].widget.attrs['class'] += ' uk-textarea'
        self.fields['apartment'].widget.attrs['class'] += ' uk-select uk-height-cotiza'
        self.fields['info_type'].widget.attrs['class'] += ' uk-select uk-height-cotiza'
        
        
