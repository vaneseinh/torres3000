"""torres3000 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from . import views

urlpatterns = [
	path('', views.HomeView.as_view(), name='index'),
	path('apartments/', views.ApartmentsView.as_view(), name='apartments'),
    path('admin/', admin.site.urls),
    path('contact/', views.ContactView.as_view(), name='contact'),
    path('cotiza/', views.CotizaView.as_view(), name='cotiza'),
    path('promotora/', views.PromotoraView.as_view(), name='promotora'),
    path('constructora/', views.ConstructoraView.as_view(), name='constructora'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
